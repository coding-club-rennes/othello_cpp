/*
** main.cpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 14:13:06 2015 Alexis Bertholom
//      update Fri Jan 19 23:44:18 2018 Lucas
// Last update Sun Feb 23 23:14:19 2020 Ghassane SEBAÏ
*/

#include <unistd.h>
#include <iostream>
#include <algorithm>
#include "SDLDisplay.hpp"
#include "Input.hpp"
#include "Colors.hpp"
#include "RNG.hpp"
bool RNG::_initialized = false;


int		main()
{
  /* Gestion de la fenêtre */
  SDLDisplay	display("Othello", 800, 800);
  /* Gestion des touches */
  Input		input;

  // Les déclarations de variables se font ici


  while (!(input.shouldExit()) && !(input.getKeyState(SDL_SCANCODE_ESCAPE)))
    {
      display.clearScreen(); // "Nettoie" la fenêtre

      /* La fenêtre fait 800*800, les cases font donc 100*100 */
      display.putRect(300, 300, 100, 100, Colors::Red);
      display.putText("Othello", 10, 10, Colors::Blue);

      // Testez par vous même si vous avez du mal à comprendre


      // Le reste de votre code ici
      // C'est ici que vous allez pouvoir coder votre Othello!


      display.refreshScreen(); // Rafraîchit la fenêtre
      input.flushEvents(); // Met à jour les touches pressées / clics de souris
    }
  return (0);
}
