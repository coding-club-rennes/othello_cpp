//
// Error.hpp for minecraft++ in /home/thepatriot/thepatriotsrepo
// 
// Made by bertho_d
// Login   <bertho_d@epitech.net>
// 
// Started on  Fri Aug  1 23:12:22 2014 bertho_d
// Last update Mon Aug  4 01:19:26 2014 bertho_d
//

#ifndef ERROR_HPP_
# define ERROR_HPP_

# include <string>

class			Error
{
public:
  Error();
  Error(const char *message);
  std::string const	&what() const;

protected:
  void			addErrorSuffix(std::string error);

protected:
  std::string		_message;
};

#endif
